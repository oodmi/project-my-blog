from datetime import timedelta

from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import redirect
from django.utils import timezone
from django.shortcuts import render, get_object_or_404
from django.db.models import Q

from .models import Post, Status, Category
from .forms import PostForm, RejectTextForm


# Create your views here.


def post_list(request):
    posts = Post.objects.filter(status=Status.objects.get(pk=2)).filter(published_date__lte=timezone.now()).order_by(
        'published_date')
    categories = Category.objects.all()
    return render(request, 'blog/post_list.html', {'posts': posts, 'categories': categories})


@login_required
def post_draft_list(request):
    posts = Post.objects.filter(status=Status.objects.get(pk=1)).filter(Q(reject_text__isnull=True) |
                                                                        Q(reject_text='')).order_by('created_date')
    categories = Category.objects.all()
    return render(request, 'blog/post_draft_list.html', {'posts': posts, 'categories': categories})


# @login_required
def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/post_detail.html', {'post': post})


@login_required
def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.created_date = timezone.now()
            if post.author.is_superuser:
                post.published_date = timezone.now()
                post.status = Status.objects.get(pk=2)
            else:
                post.status = Status.objects.get(pk=1)
            post.save()
            return redirect('blog.views.post_list')
            # return redirect('blog.views.post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'blog/post_edit.html', {'form': form})


@login_required
def post_edit(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.created_date = timezone.now()
            post.published_date = None
            if post.author.is_superuser:
                post.published_date = timezone.now()
                post.status = Status.objects.get(pk=2)
            else:
                post.status = Status.objects.get(pk=1)
            post.save()
            return redirect('blog.views.post_detail', pk=post.pk)
    else:
        form = PostForm(instance=post)
    return render(request, 'blog/post_edit.html', {'form': form})


@login_required
def post_publish(request, pk):
    post = get_object_or_404(Post, pk=pk)
    post.publish()
    return redirect('blog.views.post_detail', pk=pk)


@login_required
def post_remove(request, pk):
    post = get_object_or_404(Post, pk=pk)
    post.delete()
    return redirect('blog.views.post_list')


@login_required
def post_reject(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = RejectTextForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('blog.views.post_draft_list')
    else:
        form = RejectTextForm(instance=post)
    return render(request, 'blog/post_reject.html', {'form': form})


def user_creation(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.save()
            return redirect('django.contrib.auth.views.login')
    else:
        form = UserCreationForm()
    return render(request, 'registration/user_creation.html', {'form': form})


def post_search_categories(request, pk):
    posts = Post.objects.filter(status=Status.objects.get(pk=2)).filter(published_date__lte=timezone.now()).filter(
        category=Category.objects.get(pk=pk)).order_by('published_date')

    categories = Category.objects.all()
    return render(request, 'blog/post_list.html', {'posts': posts, 'categories': categories})


def post_search_date(request, pk):
    d = timezone.now().today() - timedelta(days=int(pk))
    posts = Post.objects.filter(status=Status.objects.get(pk=2)).filter(published_date__lte=timezone.now()).filter(
        published_date__gte=d).order_by('published_date')

    categories = Category.objects.all()
    return render(request, 'blog/post_list.html', {'posts': posts, 'categories': categories})


@login_required
def post_draft_search_categories(request, pk):
    posts = Post.objects.filter(status=Status.objects.get(pk=1)).filter(Q(reject_text__isnull=True) |
                                                                        Q(reject_text='')).filter(
        category=Category.objects.get(pk=pk)).order_by('created_date')

    categories = Category.objects.all()
    return render(request, 'blog/post_draft_list.html', {'posts': posts, 'categories': categories})


@login_required
def post_draft_search_date(request, pk):
    d = timezone.now().today() - timedelta(days=int(pk))
    posts = Post.objects.filter(status=Status.objects.get(pk=1)).filter(Q(reject_text__isnull=True) |
                                                                        Q(reject_text='')).filter(
        created_date__gte=d).order_by('published_date')

    categories = Category.objects.all()
    return render(request, 'blog/post_draft_list.html', {'posts': posts, 'categories': categories})


@login_required
def post_my(request, pk):
    posts = Post.objects.filter(author_id=pk).order_by('created_date')
    categories = Category.objects.all()
    return render(request, 'blog/post_my_list.html', {'posts': posts, 'categories': categories})


@login_required
def post_my_search_categories(request, pk_user, pk):
    posts = Post.objects.filter(author_id=pk_user).filter(category=Category.objects.get(pk=pk)).order_by(
        'created_date').order_by('published_date')

    categories = Category.objects.all()
    return render(request, 'blog/post_my_list.html', {'posts': posts, 'categories': categories})


@login_required
def post_my_search_date(request, pk_user, pk):
    d = timezone.now().today() - timedelta(days=int(pk))
    posts = Post.objects.filter(author_id=pk_user).filter(created_date__gte=d).order_by('created_date').order_by(
        'published_date')

    categories = Category.objects.all()
    return render(request, 'blog/post_my_list.html', {'posts': posts, 'categories': categories})
