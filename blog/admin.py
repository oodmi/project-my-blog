from django.contrib import admin

from .models import Post
from .models import Status
from .models import Category


admin.site.register(Post)
admin.site.register(Status)
admin.site.register(Category)
