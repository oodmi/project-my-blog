from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.post_list, name='post_list'),
    url(r'^post/(?P<pk>[0-9]+)/$', views.post_detail, name='post_detail'),

    url(r'^post/new/$', views.post_new, name='post_new'),
    url(r'^post/(?P<pk>[0-9]+)/edit/$', views.post_edit, name='post_edit'),

    url(r'^search/categories/(?P<pk>[0-9]+)/$', views.post_search_categories, name='post_search_categories'),
    url(r'^search/date/(?P<pk>[0-9]+)/$', views.post_search_date, name='post_search_date'),

    url(r'^drafts/search/categories/(?P<pk>[0-9]+)/$', views.post_draft_search_categories,
        name='post_draft_search_categories'),
    url(r'^drafts/search/date/(?P<pk>[0-9]+)/$', views.post_draft_search_date, name='post_draft_search_date'),

    url(r'^drafts/$', views.post_draft_list, name='post_draft_list'),
    url(r'^post/(?P<pk>[0-9]+)/publish/$', views.post_publish, name='post_publish'),
    url(r'^post/(?P<pk>[0-9]+)/remove/$', views.post_remove, name='post_remove'),
    url(r'^post/(?P<pk>[0-9]+)/reject/$', views.post_reject, name='post_reject'),

    url(r'^my/(?P<pk>[0-9]+)/$', views.post_my, name='post_my'),

    url(r'^my/(?P<pk_user>[0-9]+)/search/categories/(?P<pk>[0-9]+)/$', views.post_my_search_categories,
        name='post_my_search_categories'),
    url(r'^my/(?P<pk_user>[0-9]+)/search/date/(?P<pk>[0-9]+)/$', views.post_my_search_date, name='post_my_search_date'),
]
