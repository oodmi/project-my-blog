from django import forms

from .models import Post


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('category', 'title', 'text')


class RejectTextForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('category', 'title', 'reject_text')
